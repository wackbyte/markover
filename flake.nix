{
  description = "A Discord message generator that uses a Markov chain.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
  };

  outputs = { nixpkgs, flake-utils, rust-overlay, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [
          (import rust-overlay)
        ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
        rustToolchain = pkgs.rust-bin.fromRustupToolchainFile ./rust-toolchain.toml;
      in
      {
        devShell = pkgs.mkShell {
          nativeBuildInputs = [
            rustToolchain
            pkgs.pkgconfig
            pkgs.clang
            pkgs.nixpkgs-fmt
          ];

          PKG_CONFIG_PATH = "${pkgs.pkgconfig}/lib/pkgconfig";
        };
      }
    );
}
