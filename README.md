# markover

A command-line tool that generates text using a [Markov chain](https://en.wikipedia.org/wiki/Markov_chain).

It accepts a few formats so far:

* Discord data packages
* Output from [DiscordChatExporter](https://github.com/Tyrrrz/DiscordChatExporter) (probably)

## credits

Many thanks to the contributors of the [markov](https://github.com/aatxe/markov) crate,
as I adapted their code for this project.

## license

This project is in the public domain under [the Creative Commons Zero v1.0 Universal license](LICENSE).
