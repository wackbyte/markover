use {
    super::Format,
    std::{fs::File, io::Read, path::Path},
};

mod utils;

pub mod error;
pub use self::error::{Error, Result};

#[derive(Clone, Debug)]
pub struct DiscordChatExporter;

impl Format for DiscordChatExporter {
    type Error = Error;

    fn format<P>(self, path: P) -> Result<Vec<String>>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref().to_path_buf();
        if !path.exists() {
            return Err(Error::DataFileDoesNotExist { path });
        } else if path.is_dir() {
            return Err(Error::DataFileIsADirectory { path });
        }

        let mut file = match File::open(&path) {
            Ok(file) => file,
            Err(error) => {
                return Err(Error::FailedOpeningDataFile {
                    path,
                    source: error,
                });
            }
        };
        let mut buffer = String::new();
        if let Err(error) = file.read_to_string(&mut buffer) {
            return Err(Error::FailedReadingDataFile {
                path,
                source: error,
            });
        }

        let data = match json::from_str::<utils::Data>(&buffer) {
            Ok(data) => data,
            Err(error) => {
                return Err(Error::InvalidData {
                    path,
                    source: error,
                })
            }
        };
        Ok(data
            .messages
            .into_iter()
            .map(|message| message.content)
            .collect::<Vec<String>>())
    }
}
