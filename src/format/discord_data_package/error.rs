use {
    std::{io, path::PathBuf},
    thiserror::Error,
};

#[derive(Error, Debug)]
pub enum Error {
    #[error("data package at {path:?} does not exist")]
    DataPackageDoesNotExist { path: PathBuf },
    #[error("data package at {path:?} is not a directory")]
    DataPackageIsNotADirectory { path: PathBuf },

    #[error("messages directory at {path:?} is missing")]
    MessagesDirectoryMissing { path: PathBuf },
    #[error("messages directory at {path:?} is not a directory")]
    MessagesDirectoryIsNotADirectory { path: PathBuf },

    #[error("message data file at {path:?} is missing")]
    MessageDataFileMissing { path: PathBuf },
    #[error("message data file at {path:?} is a directory")]
    MessageDataFileIsADirectory { path: PathBuf },
    #[error("failed opening message data file at {path:?}: {source}")]
    FailedOpeningMessageDataFile {
        path: PathBuf,
        #[source]
        source: csv::Error,
    },

    #[error("message data at {path:?} is invalid: {source}")]
    InvalidMessageData {
        path: PathBuf,
        #[source]
        source: csv::Error,
    },
    #[error("message data at {path:?} is missing a content field")]
    MalformedMessageData { path: PathBuf },

    #[error("io error: {0}")]
    Io(#[from] io::Error),
    #[error("csv error: {0}")]
    Csv(#[from] csv::Error),
}

pub type Result<T> = std::result::Result<T, Error>;
