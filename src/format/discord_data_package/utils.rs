use {
    super::error::{Error, Result},
    csv::StringRecord,
    rayon::prelude::*,
    std::{fs::DirEntry, io, path::Path},
};

/// The index of the content field in Discord's CSV message data.
const MESSAGE_CONTENT_FIELD: usize = 2;

/// Load each message chunk from `{package}/messages`.
///
/// Uses `rayon` to load each message chunk over multiple threads.
pub fn load_messages<P>(path: P) -> Result<Vec<String>>
where
    P: AsRef<Path>,
{
    Ok(path
        .as_ref()
        .read_dir()?
        .collect::<io::Result<Vec<DirEntry>>>()?
        .into_par_iter()
        .filter_map(|entry| match entry.file_type() {
            Ok(file_type) => {
                if file_type.is_dir() {
                    Some(load_message_chunk(entry.path()))
                } else {
                    None
                }
            }
            Err(error) => Some(Err(Error::Io(error))),
        })
        .collect::<Result<Vec<Vec<String>>>>()?
        .into_iter()
        .flatten()
        .collect::<Vec<String>>())
}

/// Load the content of each message in a message chunk.
pub fn load_message_chunk<P>(path: P) -> Result<Vec<String>>
where
    P: AsRef<Path>,
{
    let path = path.as_ref().join("messages.csv");
    if !path.exists() {
        return Err(Error::MessageDataFileMissing { path });
    } else if path.is_dir() {
        return Err(Error::MessageDataFileIsADirectory { path });
    }

    let mut data = match csv::ReaderBuilder::new().has_headers(true).from_path(&path) {
        Ok(data) => data,
        Err(error) => {
            return Err(Error::FailedOpeningMessageDataFile {
                path,
                source: error,
            });
        }
    };

    match data.records().collect::<csv::Result<Vec<StringRecord>>>() {
        Ok(records) => match records
            .into_iter()
            .map(|record| record.get(MESSAGE_CONTENT_FIELD).map(ToOwned::to_owned))
            .collect::<Option<Vec<String>>>()
        {
            Some(messages) => Ok(messages),
            None => Err(Error::MalformedMessageData { path }),
        },
        Err(error) => Err(Error::InvalidMessageData {
            path,
            source: error,
        }),
    }
}
