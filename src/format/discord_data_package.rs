use {super::Format, std::path::Path};

mod utils;

pub mod error;
pub use self::error::{Error, Result};

#[derive(Clone, Debug)]
pub struct DiscordDataPackage;

impl Format for DiscordDataPackage {
    type Error = Error;

    fn format<P>(self, path: P) -> Result<Vec<String>>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref().to_path_buf();
        if !path.exists() {
            return Err(Error::DataPackageDoesNotExist { path });
        } else if !path.is_dir() {
            return Err(Error::DataPackageIsNotADirectory { path });
        }

        let path = path.join("messages");
        if !path.exists() {
            return Err(Error::MessagesDirectoryMissing { path });
        } else if !path.is_dir() {
            return Err(Error::MessagesDirectoryIsNotADirectory { path });
        }

        Ok(utils::load_messages(path)?)
    }
}
