use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Data {
    pub messages: Vec<Message>,
}

// Thanks to a quirk in serde where unknown fields will be ignored,
// I can just say exactly what I need here rather than dealing with `HashMap`s and `Value`s!
#[derive(Clone, Debug, Deserialize)]
pub struct Message {
    pub content: String,
}
