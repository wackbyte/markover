use {
    std::{io, path::PathBuf},
    thiserror::Error,
};

#[derive(Error, Debug)]
pub enum Error {
    #[error("data file at {path:?} does not exist")]
    DataFileDoesNotExist { path: PathBuf },
    #[error("data file at {path:?} is a directory")]
    DataFileIsADirectory { path: PathBuf },
    #[error("failed opening data file at {path:?}: {source}")]
    FailedOpeningDataFile {
        path: PathBuf,
        #[source]
        source: io::Error,
    },
    #[error("failed reading data file at {path:?}: {source}")]
    FailedReadingDataFile {
        path: PathBuf,
        #[source]
        source: io::Error,
    },

    #[error("data at {path:?} is invalid: {source}")]
    InvalidData {
        path: PathBuf,
        #[source]
        source: json::Error,
    },

    #[error("io error: {0}")]
    Io(#[from] io::Error),
    #[error("json error: {0}")]
    Json(#[from] json::Error),
}

pub type Result<T> = std::result::Result<T, Error>;
