#![deny(clippy::pedantic)]

extern crate serde_json as json;

use {
    crate::{
        chain::Chain,
        error::{ParseInputError, Result},
        format::Format,
    },
    rayon::iter::{ParallelBridge, ParallelIterator},
    std::{collections::HashSet, num::NonZeroUsize, path::PathBuf, process, str::FromStr},
    structopt::StructOpt,
};

mod chain;
mod error;
mod format;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum Input {
    DiscordDataPackage,
    DiscordChatExporter,
}

impl FromStr for Input {
    type Err = ParseInputError;

    fn from_str(input: &str) -> std::result::Result<Self, Self::Err> {
        match input {
            "discord-data-package" | "data-package" => Ok(Self::DiscordDataPackage),
            "discord-chat-exporter" | "chat-exporter" => Ok(Self::DiscordChatExporter),
            _ => Err(ParseInputError {
                input: input.to_owned(),
            }),
        }
    }
}

/// Generate text using a Markov chain.
///
/// Pass in the kind of data you have and a path to it.
/// Each output is separated by two newlines.
#[derive(StructOpt, Clone, Debug)]
struct Opt {
    /// The input type.
    ///
    /// Possible values:
    /// `discord-data-package`, `data-package`,
    /// `discord-chat-exporter`, `chat-exporter`
    #[structopt(short = "t", long = "type")]
    input: String,

    /// The path to the input data.
    #[structopt(short, long)]
    path: PathBuf,

    /// The number of results to generate.
    #[structopt(short, long)]
    count: usize,

    /// The order of the Markov chain.
    /// Generally, this affects how close to the original data the output is.
    ///
    /// Higher values may take longer to process.
    #[structopt(short, long, default_value = "1")]
    order: NonZeroUsize,

    /// Filter out any outputs that were copied exactly from the input.
    ///
    /// May cause an infinite loop due to a known issue.
    #[structopt(short = "d", long)]
    no_duplicates: bool,
}

fn run() -> Result<()> {
    let opt = Opt::from_args();

    let input = opt.input.parse::<Input>()?;
    let data = match input {
        Input::DiscordDataPackage => format::DiscordDataPackage.format(opt.path)?,
        Input::DiscordChatExporter => format::DiscordChatExporter.format(opt.path)?,
    };

    let mut chain = Chain::of_order(opt.order);
    for input in &data {
        chain.feed_text(input);
    }

    if opt.no_duplicates {
        let mut generator = chain.text_iter();

        let mut outputs = HashSet::with_capacity(opt.count);
        while outputs.len() < opt.count {
            // FIXME: if we exhaust all possible states of the Markov chain, this loop may never terminate.
            // Also, it may just take forever because we're dealing with randomness.

            let generated = generator.next().unwrap();
            if !data.contains(&generated) {
                outputs.insert(generated);
            }
        }

        for output in &outputs {
            println!("{}\n", output);
        }
    } else {
        // Just by using `par_bridge()` here, we can instantly use every single core.
        chain
            .text_iter_for(opt.count)
            .par_bridge()
            .for_each(|output| {
                println!("{}\n", output);
            });
    }

    Ok(())
}

fn main() {
    if let Err(error) = run() {
        eprintln!("{}", error);
        process::exit(1);
    } else {
        process::exit(0);
    }
}
