use {
    crate::format::{discord_chat_exporter, discord_data_package},
    thiserror::Error,
};

#[derive(Error, Clone, Debug, Eq, PartialEq)]
#[error("unknown input type \"{input}\"")]
pub struct ParseInputError {
    pub input: String,
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("{0}")]
    ParseInputError(#[from] ParseInputError),
    #[error("{0}")]
    DiscordDataPackage(#[from] discord_data_package::Error),
    #[error("{0}")]
    DiscordChatExporter(#[from] discord_chat_exporter::Error),
}

pub type Result<T> = std::result::Result<T, Error>;
