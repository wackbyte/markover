use {
    rand::{thread_rng, Rng},
    std::{
        borrow::ToOwned,
        collections::{hash_map::Entry, HashMap},
        iter::Map,
        num::NonZeroUsize,
    },
};

pub type Token = Option<String>;

/// A [Markov chain](https://en.wikipedia.org/wiki/Markov_chain).
#[derive(PartialEq, Debug)]
pub struct Chain {
    map: HashMap<Vec<Token>, HashMap<Token, usize>>,
    order: NonZeroUsize,
}

impl Default for Chain {
    fn default() -> Self {
        Self::new()
    }
}

impl Chain {
    /// Constructs a new Markov chain.
    pub fn new() -> Self {
        Self::of_order(NonZeroUsize::new(1).unwrap())
    }

    /// Creates a new Markov chain of the specified order. The order is the number of previous
    /// tokens to use for each mapping in the chain. Higher orders mean that the generated text
    /// will more closely resemble the training set. Increasing the order can yield more realistic
    /// output, but typically at the cost of requiring more training data.
    pub fn of_order(order: NonZeroUsize) -> Self {
        Self {
            map: {
                let mut map = HashMap::new();
                map.insert(vec![None; order.get()], HashMap::new());
                map
            },
            order,
        }
    }

    /// Determines whether or not the chain is empty. A chain is considered empty if nothing has
    /// been fed into it.
    pub fn is_empty(&self) -> bool {
        self.map[&vec![None; self.order.get()]].is_empty()
    }

    /// Feeds the chain a collection of tokens. This operation is `O(n)` where `n` is the number of
    /// tokens to be fed into the chain.
    pub fn feed<S>(&mut self, tokens: S) -> &mut Self
    where
        S: AsRef<[String]>,
    {
        let tokens = tokens.as_ref();
        if tokens.is_empty() {
            return self;
        }

        let mut food = vec![None; self.order.get()];
        food.extend(tokens.iter().map(|token| Some(token.clone())));
        food.push(None);
        for p in food.windows(self.order.get() + 1) {
            self.map
                .entry(p[0..self.order.get()].to_vec())
                .or_insert_with(HashMap::new);
            self.map
                .get_mut(&p[0..self.order.get()].to_vec())
                .unwrap()
                .add(p[self.order.get()].clone());
        }

        self
    }

    /// Generates a collection of tokens from the chain. This operation is `O(mn)` where `m` is the
    /// length of the generated collection, and `n` is the number of possible states from a given
    /// state.
    pub fn generate(&self) -> Vec<String> {
        let mut output = Vec::new();
        let mut current = vec![None; self.order.get()];
        loop {
            let next = self.map[&current].next();

            current = current[1..self.order.get()].to_vec();
            current.push(next.clone());
            if let Some(next) = next {
                output.push(next);
            }

            if current[self.order.get() - 1].is_none() {
                break;
            }
        }

        output
    }

    /// Generates a collection of tokens from the chain, starting with the given token. This
    /// operation is O(mn) where m is the length of the generated collection, and n is the number
    /// of possible states from a given state. This returns an empty vector if the token is not
    /// found.
    pub fn generate_from_token(&self, token: String) -> Vec<String> {
        let mut current = vec![None; self.order.get() - 1];
        current.push(Some(token.clone()));
        if !self.map.contains_key(&current) {
            return Vec::new();
        }

        let mut output = vec![token];
        loop {
            let next = self.map[&current].next();

            current = current[1..self.order.get()].to_vec();
            current.push(next.clone());
            if let Some(next) = next {
                output.push(next);
            }

            if current[self.order.get() - 1].is_none() {
                break;
            }
        }

        output
    }

    /// Produces an infinite iterator of generated token collections.
    pub fn iter(&self) -> InfiniteChainIterator {
        InfiniteChainIterator { chain: self }
    }

    /// Produces an iterator for the specified number of generated token collections.
    pub fn iter_for(&self, size: usize) -> SizedChainIterator {
        SizedChainIterator { chain: self, size }
    }
}

impl Chain {
    /// Feeds text into the chain.
    pub fn feed_text(&mut self, string: &str) -> &mut Self {
        self.feed(&string.split(' ').map(ToOwned::to_owned).collect::<Vec<_>>())
    }

    /// Generates random text.
    pub fn generate_text(&self) -> String {
        self.generate().join(" ")
    }

    /// Generates random text starting with the desired token. This returns an empty
    /// string if the token is not found.
    pub fn generate_text_from_token(&self, string: &str) -> String {
        self.generate_from_token(string.to_owned()).join(" ")
    }

    /// Produces an infinite iterator of generated text.
    pub fn text_iter(&self) -> InfiniteTextIterator {
        self.iter().map(|v| v.join(" "))
    }

    /// Produces a sized iterator of generated text.
    pub fn text_iter_for(&self, size: usize) -> SizedTextIterator {
        self.iter_for(size).map(|v| v.join(" "))
    }
}

/// A sized iterator over a Markov chain of text.
pub type SizedTextIterator<'a> = Map<SizedChainIterator<'a>, fn(Vec<String>) -> String>;

/// A sized iterator over a Markov chain.
pub struct SizedChainIterator<'a> {
    chain: &'a Chain,
    size: usize,
}

impl<'a> Iterator for SizedChainIterator<'a> {
    type Item = Vec<String>;

    fn next(&mut self) -> Option<Vec<String>> {
        if self.size > 0 {
            self.size -= 1;
            Some(self.chain.generate())
        } else {
            None
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.size, Some(self.size))
    }
}

/// An infinite iterator over a Markov chain of text.
pub type InfiniteTextIterator<'a> = Map<InfiniteChainIterator<'a>, fn(Vec<String>) -> String>;

/// An infinite iterator over a Markov chain.
pub struct InfiniteChainIterator<'a> {
    chain: &'a Chain,
}

impl<'a> Iterator for InfiniteChainIterator<'a> {
    type Item = Vec<String>;

    fn next(&mut self) -> Option<Vec<String>> {
        Some(self.chain.generate())
    }
}

/// A collection of states for the Markov chain.
pub trait States {
    /// Adds a state to this states collection.
    fn add(&mut self, token: Token);

    /// Gets the next state from this collection of states.
    fn next(&self) -> Token;
}

impl States for HashMap<Token, usize> {
    fn add(&mut self, token: Token) {
        match self.entry(token) {
            Entry::Occupied(mut e) => *e.get_mut() += 1,
            Entry::Vacant(e) => {
                e.insert(1);
            }
        }
    }

    fn next(&self) -> Token {
        let mut sum = 0;
        for &value in self.values() {
            sum += value;
        }

        let mut rng = thread_rng();
        let cap = rng.gen_range(0..sum);
        sum = 0;
        for (key, &value) in self.iter() {
            sum += value;
            if sum > cap {
                return key.clone();
            }
        }

        unreachable!("The random number generator failed.")
    }
}

#[cfg(test)]
mod test {
    use {super::Chain, std::num::NonZeroUsize};

    #[test]
    fn new() {
        Chain::new();
    }

    #[test]
    fn is_empty() {
        let mut chain = Chain::new();
        assert!(chain.is_empty());
        chain.feed(vec!["a".to_owned(), "b".to_owned(), "c".to_owned()]);
        assert!(!chain.is_empty());
    }

    #[test]
    fn feed() {
        let mut chain = Chain::new();
        chain
            .feed(vec!["a".to_owned(), "b".to_owned(), "c".to_owned()])
            .feed(vec!["b".to_owned(), "d".to_owned()]);
    }

    #[test]
    fn generate() {
        let mut chain = Chain::new();
        chain
            .feed(vec!["a".to_owned(), "b".to_owned(), "c".to_owned()])
            .feed(vec!["b".to_owned(), "d".to_owned()]);
        let v = chain.generate();
        assert!([
            vec!["a".to_owned(), "b".to_owned(), "c".to_owned()],
            vec!["a".to_owned(), "b".to_owned(), "d".to_owned()],
            vec!["b".to_owned(), "c".to_owned()],
            vec!["b".to_owned(), "d".to_owned()]
        ]
        .contains(&v));
    }

    #[test]
    fn generate_for_higher_order() {
        let mut chain = Chain::of_order(NonZeroUsize::new(2).unwrap());
        chain
            .feed(vec!["a".to_owned(), "b".to_owned(), "c".to_owned()])
            .feed(vec![
                "z".to_owned(),
                "a".to_owned(),
                "b".to_owned(),
                "d".to_owned(),
            ]);
        let v = chain.generate();
        assert!([
            vec!["a".to_owned(), "b".to_owned(), "c".to_owned()],
            vec!["a".to_owned(), "b".to_owned(), "d".to_owned()],
            vec![
                "z".to_owned(),
                "a".to_owned(),
                "b".to_owned(),
                "c".to_owned()
            ],
            vec![
                "z".to_owned(),
                "a".to_owned(),
                "b".to_owned(),
                "d".to_owned()
            ]
        ]
        .contains(&v));
    }

    #[test]
    fn generate_from_token() {
        let mut chain = Chain::new();
        chain
            .feed(vec!["a".to_owned(), "b".to_owned(), "c".to_owned()])
            .feed(vec!["b".to_owned(), "d".to_owned()]);
        let v = chain.generate_from_token("b".to_owned());
        assert!([
            vec!["b".to_owned(), "c".to_owned()],
            vec!["b".to_owned(), "d".to_owned()]
        ]
        .contains(&v));
    }

    #[test]
    fn generate_from_unfound_token() {
        let mut chain = Chain::new();
        chain
            .feed(vec!["a".to_owned(), "b".to_owned(), "c".to_owned()])
            .feed(vec!["b".to_owned(), "d".to_owned()]);
        let v: Vec<_> = chain.generate_from_token("z".to_owned());
        assert!(v.is_empty());
    }

    #[test]
    fn iter() {
        let mut chain = Chain::new();
        chain
            .feed(vec!["a".to_owned(), "b".to_owned(), "c".to_owned()])
            .feed(vec!["b".to_owned(), "d".to_owned()]);
        assert_eq!(chain.iter().size_hint().1, None);
    }

    #[test]
    fn iter_for() {
        let mut chain = Chain::new();
        chain
            .feed(vec!["a".to_owned(), "b".to_owned(), "c".to_owned()])
            .feed(vec!["b".to_owned(), "d".to_owned()]);
        assert_eq!(chain.iter_for(5).count(), 5);
    }

    #[test]
    fn feed_text() {
        let mut chain = Chain::new();
        chain.feed_text("I like cats and dogs");
    }

    #[test]
    fn generate_text() {
        let mut chain = Chain::new();
        chain.feed_text("I like cats").feed_text("I hate cats");
        assert!(["I like cats", "I hate cats"].contains(&&chain.generate_text()[..]));
    }

    #[test]
    fn generate_text_from_token() {
        let mut chain = Chain::new();
        chain.feed_text("I like cats").feed_text("cats are cute");
        assert!(["cats", "cats are cute"].contains(&&chain.generate_text_from_token("cats")[..]));
    }

    #[test]
    fn generate_text_from_token_higher_order() {
        let mut chain = Chain::of_order(NonZeroUsize::new(2).unwrap());
        chain.feed_text("I like cats").feed_text("cats are cute");
        println!("{:?}", chain.generate_text_from_token("cats"));
        assert!(["cats", "cats are cute"].contains(&&chain.generate_text_from_token("cats")[..]));
    }

    #[test]
    fn generate_text_from_unfound_token() {
        let mut chain = Chain::new();
        chain.feed_text("I like cats").feed_text("cats are cute");
        assert_eq!(chain.generate_text_from_token("test"), "");
    }

    #[test]
    fn text_iter() {
        let mut chain = Chain::new();
        chain.feed_text("I like cats and I like dogs");
        assert_eq!(chain.text_iter().size_hint().1, None);
    }

    #[test]
    fn text_iter_for() {
        let mut chain = Chain::new();
        chain.feed_text("I like cats and I like dogs");
        assert_eq!(chain.text_iter_for(5).count(), 5);
    }
}
