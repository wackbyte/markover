use std::path::Path;

pub trait Format {
    type Error;

    fn format<P>(self, path: P) -> Result<Vec<String>, Self::Error>
    where
        P: AsRef<Path>;
}

pub mod discord_chat_exporter;
pub mod discord_data_package;

pub use self::{
    discord_chat_exporter::DiscordChatExporter, discord_data_package::DiscordDataPackage,
};
